import React from "react";
import InputGroupSelect from "./InputGroupSelect";
import InputGroup from "./InputGroup";
import Label from './Label';

export default function EmploymentInformation({state, setState, props}) {
    return (
        <div className="border">
            <div className="font-weight-bold text-xl bg-black text-white py-4 px-4">
                Employment Information
            </div>
            <div className="grid md:grid-cols-6 gap-4 p-4">
                {[
                    {
                        type: "text",
                        label: "Position You Are Applying For",
                        name: "position",
                        className: "col-span-2",
                    },
                    {
                        type: "date",
                        label: "Date Available For Work",
                        name: "available_date",
                        className: "col-span-2",
                    },
					{
                        type: "number",
                        label: "Expected Salary",
                        name: "expected_salary",
                        className: "col-span-2",
                    },
                ].map(({ type, label, name, className, options }) => {
                    switch (type) {
                        case "select":
                            return (
							<InputGroupSelect
                                    key={name.toString()}
                                    className={className}
                                    label={label}
                                    name={name}
									options={options}
									errors={props.errors}
                                    handleChange={(e) =>
                                        setState({
                                            ...state,
                                            [name]: e.target.value,
                                        })
                                    }
                                />
                            );
                            break;
                        default:
                            return (
                                <InputGroup
                                    key={name.toString()}
                                    type={type}
                                    className={className}
                                    label={label}
                                    name={name}
									errors={props.errors}
                                    handleChange={(e) =>
                                        setState({
                                            ...state,
                                            [name]: e.target.value,
                                        })
                                    }
                                />
                            );
                            break;
                    }
                })}
            </div>
        </div>
    );
}
