import React from 'react'

export default function Select({name, className, children, handleChange}) {
	
	return (
		<select name={name} className={"border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm "+className} onChange={(e) => handleChange(e)}>
			{children}
		</select>
	)
}
