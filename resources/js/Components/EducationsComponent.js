import React, {useContext} from "react";
import Input from "./Input";
import Button from "./Button";
import { ErrorsContext } from "@/Pages/Employment";
import _ from 'lodash';
import update from "react-addons-update";

export default function EducationsComponent({state, setState, props, handleEducationAddRow, handleEducationDeleteRow}) {
	const errors = useContext(ErrorsContext)
	console.log("education errors", !_.isEmpty(errors) && errors['educations.0.school_name'])
    return (
        <div className="border">
            <div className="font-weight-bold text-xl bg-black text-white py-4 px-4">
                Educations
            </div>
            <div className="p-4">
                <table className="table-auto w-full">
                    <thead>
                        <tr>
                            <th>School Name</th>
                            <th>Location</th>
                            <th>Year Attended</th>
                            <th>Degree Received</th>
                            <th>Major</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {state.educations.map((row, index) => (
                            <tr key={index.toString()}>
                                {[
                                    "school_name",
                                    "location",
                                    "year",
                                    "degree",
                                    "major",
                                ].map((field) => (
                                    <td key={field.toString()}>
                                        <Input
                                            className="w-full rounded-none"
                                            name={`educations[][${field}]`}
                                            value={
                                                state.educations[index][field]
                                            }
											errors={errors[`educations.${index}.${field}`]}
                                            handleChange={(e) =>
                                                setState(
                                                    update(state, {
                                                        educations: {
                                                            [index]: {
                                                                [field]: {
                                                                    $set: e
                                                                        .target
                                                                        .value,
                                                                },
                                                            },
                                                        },
                                                    })
                                                )
                                            }
                                        />
                                    </td>
                                ))}
                                <td>
                                    {index + 1 == state.educations.length && (
                                        <div className="inline-block">
                                            <Button
                                                className="bg-blue-500 block justify-center "
                                                handleClick={
                                                    handleEducationAddRow
                                                }
                                            >
                                                +
                                            </Button>
                                            {index != 0 && (
                                                <Button
                                                    className="bg-red-500 block justify-center"
                                                    handleClick={
                                                        handleEducationDeleteRow
                                                    }
                                                >
                                                    -
                                                </Button>
                                            )}
                                        </div>
                                    )}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
