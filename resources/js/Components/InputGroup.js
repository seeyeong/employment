import React, { useContext } from "react";
import Label from "./Label";
import Input from "./Input";
import { ErrorsContext } from "@/Pages/Employment";

export default function InputGroup({label, className, name, handleChange}) {
	const errors = useContext(ErrorsContext)
	console.log("errors", errors[name])
    return (
        <div className={`input-group  ${className}`}>
            <Label value={label} />
            <Input
                className="w-full"
                name={name}
				handleChange={handleChange}
				errors={errors[name]}
            />
        </div>
    );
}
