import React from "react";
import InputGroupSelect from "./InputGroupSelect";
import InputGroup from "./InputGroup";
import Label from './Label';
import Checkbox from './Checkbox';
import { citites } from "@/cities";

const states = [
    "Johor",
    "Kedah",
    "Kelantan",
    "Melaka",
    "Negeri Sembilan",
    "Pahang",
    "Perak",
    "Perlis",
    "Pulau Pinang",
    "Sarawak",
    "Selangor",
    "Terengganu",
    "Kuala Lumpur",
    "Labuan",
    "Sabah",
    "Putrajaya",
];

export default function PersonalInformation({state, setState, props}) {
    return (
        <div className="border">
            <div className="font-weight-bold text-xl bg-black text-white py-4 px-4">
                Personal Information
            </div>
            <div className="grid md:grid-cols-6 gap-4 p-4">
                {[
                    {
                        type: "text",
                        label: "Last Name",
                        name: "last_name",
                        className: "col-span-2",
                    },
                    {
                        type: "text",
                        label: "Middle Name",
                        name: "middle_name",
                        className: "col-span-2",
                    },
                    {
                        type: "text",
                        label: "First Name",
                        name: "first_name",
                        className: "col-span-2",
                    },
                    {
                        type: "text",
                        label: "Address",
                        name: "address",
                        className: "col-span-3",
                    },
                    {
                        type: "select",
                        label: "State",
                        name: "state",
                        className: "col-span-2",
                        options: states,
                    },
                    {
                        type: "select",
                        label: "City",
                        name: "city",
                        className: "col-span-2",
                        options: citites[state.state],
                    },
                    {
                        type: "number",
                        label: "Postcode",
                        name: "postcode",
                        className: "",
                    },
                ].map(({ type, label, name, className, options }) => {
                    switch (type) {
                        case "select":
                            return (
							<InputGroupSelect
                                    key={name.toString()}
                                    className={className}
                                    label={label}
                                    name={name}
									options={options}
									errors={props.errors}
                                    handleChange={(e) =>
                                        setState({
                                            ...state,
                                            [name]: e.target.value,
                                        })
                                    }
                                />
                            );
                            break;
                        default:
                            return (
                                <InputGroup
                                    key={name.toString()}
                                    type={type}
                                    className={className}
                                    label={label}
                                    name={name}
									errors={props.errors}
                                    handleChange={(e) =>
                                        setState({
                                            ...state,
                                            [name]: e.target.value,
                                        })
                                    }
                                />
                            );
                            break;
                    }
                })}
            </div>
            <div className="grid p-4">
                <div className="input-group">
                    <Label value="Are you U.S Citizen?" />
                    <Checkbox
                        handleChange={(e) =>
                            setState({
                                ...state,
                                us_citizen: e.target.checked,
                            })
                        }
                    />
                </div>
                <div className="input-group">
                    <Label value="Have you ever been convited of a felony?" />
                    <Checkbox
                        handleChange={(e) =>
                            setState({
                                ...state,
                                felony: e.target.checked,
                            })
                        }
                    />
                </div>
                <div className="input-group">
                    <Label value="If selected for employment are you willing to submit to a pre-emploment drug screening test?" />
                    <Checkbox
                        handleChange={(e) =>
                            setState({
                                ...state,
                                drug_test: e.target.checked,
                            })
                        }
                    />
                </div>
            </div>
        </div>
    );
}
