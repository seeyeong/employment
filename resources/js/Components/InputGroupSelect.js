import React from "react";
import Label from "./Label";
import Select from "./Select";

export default function InputGroupSelect({label, name, handleChange, options, children, errors}) {
    return (
        <div className="input-group">
            <Label value={label} />
            <Select
                name={name}
                className="w-full"
                handleChange={handleChange}
				errors={errors}
            >
				{
					options ? options.map((option) => (
						<option key={option.toString()}>{option}</option>
					)) : children
				}
            </Select>
        </div>
    );
}
