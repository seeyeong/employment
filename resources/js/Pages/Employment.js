import React, { createContext, useContext, useState } from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import Label from "@/Components/Label";
import Input from "@/Components/Input";
import { useSvgDrawing } from "react-hooks-svgdrawing";
import Button from "@/Components/Button";
import update from "react-addons-update";
import PersonalInformation from "@/Components/PersonalInformation";
import { Inertia } from "@inertiajs/inertia";
import { Alert } from "antd";
import _ from "lodash";
import EmploymentInformation from "@/Components/EmploymentInformation";
import EducationsComponent from "@/Components/EducationsComponent";

//create errors context for all children component.
export const ErrorsContext = createContext({});

export default function Employment(props) {
    const [state, setState] = useState({
        position: "",
        available_date: "",
        expected_salary: "",
        last_name: "",
        middle_name: "",
        first_name: "",
        address: "",
        state: "",
        city: "",
        postcode: "",
        us_citizen: false,
        felony: false,
        drug_test: false,
        education_row: {
            school_name: "",
            location: "",
            year: "",
            degree: "",
            major: "",
        },
        educations: [
            {
                school_name: "",
                location: "",
                year: "",
                degree: "",
                major: "",
            },
        ],
    });

    const [renderRef, draw] = useSvgDrawing();

    const handleEducationAddRow = (e) => {
        e.preventDefault();
        setState({
            ...state,
            educations: [...state.educations, state.education_row],
        });
    };

    const handleEducationDeleteRow = (e) => {
        e.preventDefault();
        //if length greater than 2 then can remove last element
        if (state.educations.length >= 2) {
            let new_educations = state.educations.pop();
            setState({
                ...state,
                educations: state.educations,
            });
        }
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        Inertia.post("employment", state);
    };

    return (
        <ErrorsContext.Provider value={props.errors}>
            <Authenticated
                auth={props.auth}
                errors={props.errors}
                header={
                    <h2 className="font-semibold text-xl text-gray-800 leading-tight">
                        Employment
                    </h2>
                }
            >
                <Head title="Employment" />

                <div className="max-w-7xl mx-auto p-4">
                    {!_.isEmpty(props.errors) && (
                        <Alert
                            message="You have errors"
                            type="error"
                            closable
                            onClose={() => console.log("alert close")}
                        />
                    )}
                </div>
                <div className="py-12">
                    <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white p-4">
                        <form
                            className="space-y-2"
                            onSubmit={(e) => handleSubmit(e)}
                        >
                            <EmploymentInformation
                                state={state}
                                setState={setState}
                                props={props}
                            />
                            <PersonalInformation
                                state={state}
                                setState={setState}
                                props={props}
                            />
                            <EducationsComponent
                                state={state}
                                setState={setState}
                                props={props}
								handleEducationAddRow={handleEducationAddRow}
								handleEducationDeleteRow={handleEducationDeleteRow}
                            />
                            <div
                                className="border-dashed border-2"
                                style={{ width: 500, height: 300 }}
                                ref={renderRef}
                            />

                            <Button>Submit</Button>
                        </form>
                    </div>
                </div>
            </Authenticated>
        </ErrorsContext.Provider>
    );
}
