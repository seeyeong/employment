<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Education;
use App\Models\Employment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class EmploymentController extends Controller
{
    public function index()
	{
		// return Employment::with('educations')->get();
		return Inertia::render('Employment');
	}

	public function store(Request $request)
	{
		$request->validate([
			'position' => ['required'],
			'available_date' => ['required', 'date'],
			'expected_salary' => ['required', 'numeric'],
			'first_name' => ['required'],
			'last_name' => ['required'],
			'address' => ['required'],
			'state' => ['required'],
			'city' => ['required'],
			'postcode' => ['required', 'numeric'],
			'educations.*.school_name' => ['required'],
			'educations.*.year' => ['required'],
			'educations.*.major' => ['required'],
		]);

		Employment::create($request->except(['educations', 'education_row']))
				->educations()
				->createMany($request->only('educations')['educations']);

		return Redirect::route('employment');
	}
}
