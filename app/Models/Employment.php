<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employment extends Model
{
    use HasFactory;

	protected $guarded = []; //to accept all fillable

	/**
	 * Get all of the education for the Employment
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function educations()
	{
		return $this->hasMany(Education::class);
	}
}
