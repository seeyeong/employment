<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    use HasFactory;

	protected $guarded = [];
	
	/**
	 * Get the employment that owns the Education
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function employment()
	{
		return $this->belongsTo(Employment::class);
	}
}
