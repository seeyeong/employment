<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employments', function (Blueprint $table) {
            $table->id();
			$table->string('position');
			$table->date('available_date')->nullable();
			$table->decimal('expected_salary', 13, 2)->nullable()->default(1200);
			$table->string('first_name', 100);
			$table->string('middle_name', 20)->nullable();
			$table->string('last_name', 100);
			$table->string('address')->nullable();
			$table->string('state')->nullable();
			$table->string('city')->nullable();
			$table->string('postcode')->nullable();
			$table->boolean('us_citizen')->default(false);
			$table->boolean('felony')->default(false);
			$table->boolean('drug_test')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employments');
    }
}
